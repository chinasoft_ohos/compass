// xxx.js
import sensor from '@system.sensor';

var el = null;
var ctx = null;
var lin_el = null;
var lin_ctx = null;

export default {
    props: {
        compWidth: {
            default: 300,
        },
        compHeight: {
            default: 300,
        }
    },
    data() {
        return {
            width: this.compWidth,
            height: this.compHeight,
            outsideRadius: 0,
            insideRadius: 0,
            fontSize: '',
            numberSize: '',
            isFirst: true,
            textArrs: ["北", "东", "南", "西"],
            numberArrs: ['0', '30', '60', '90', '120', '150', '180', '210', '240', '270', '300', '330'],
            animation: '',
            angle: 0,
        };
    },
    onInit() {

    },
    onAttached() {
        this.subscribeCompassSensor();
    },
    onPageShow() {
        this.getWindowSize();
        this.initCanvas();
    },
    initCanvas() {
        el = this.$element('compassCanvas');
        ctx = el.getContext('2d', {
            antialias: true
        });

        lin_el = this.$refs.linecanvas;
        lin_ctx = lin_el.getContext('2d', {
            antialias: true
        });
        if (this.isFirst) {
            ctx.translate(this.width / 2, this.height / 2);
            lin_ctx.translate(this.width / 2, this.height / 2);
            this.drawCircle(this.outsideRadius, 'rgb(245,245,245)');
            this.drawCircle(this.insideRadius, 'rgb(255,255,255)');
            this.drawArcNumber();
            this.drawArcText();
            this.drawArcLine();
            this.drawArrowLine();
        }
        this.isFirst = false;
    },
    getWindowSize() {
        this.outsideRadius = this.width / 2 - this.width / 10;
        this.insideRadius = this.outsideRadius - this.width / 6;
    },
    drawCircle(radius, color) {
        ctx.beginPath();
        ctx.fillStyle = color;
        ctx.strokeStyle = color;
        ctx.arc(0, 0, radius, 0, 6.28);
        ctx.fill();
    },
    drawArcText() {
        ctx.beginPath();
        this.fontSize = this.width / 20;
        ctx.font = this.fontSize + 'px' + ' sans-serif';
        for (let i = 0; i < this.textArrs.length; i++) {
            if (i == 0) {
                ctx.fillStyle = 'rgb(255,0,0)';
                this.drawTriangle();
            } else {
                ctx.fillStyle = 'rgb(0,0,0)';
            }
            let text = this.textArrs[i];
            let textWidth = ctx.measureText(text).width;
            ctx.fillText(text, -textWidth / 2, -this.width / 13);
            ctx.stroke();
            ctx.rotate(90 * Math.PI / 180);
        }
    },
    drawTriangle() {
        ctx.beginPath();
        ctx.fillStyle = 'rgb(255,0,0)';
        ctx.moveTo(0, -this.width / 5);
        ctx.lineTo(this.width / 25, -this.width / 5 + this.width / 15);
        ctx.lineTo(0, -this.width / 5 + this.width / 20);
        ctx.lineTo(-this.width / 25, -this.width / 5 + this.width / 15);
        ctx.closePath();
        ctx.fill();
    },
    drawArcNumber() {
        ctx.beginPath();
        this.numberSize = this.width / 30;
        ctx.font = this.numberSize + 'px' + ' sans-serif';
        for (var i = 0; i < this.numberArrs.length; i++) {
            if (i == 0) {
                ctx.fillStyle = 'red';
            } else {
                ctx.fillStyle = '#AF000000';
                ctx.fillStyle = 'rgb(0,0,0)';
            }
            let number = this.numberArrs[i];
            let textWidth = ctx.measureText(number).width;
            ctx.fillText(number, -textWidth / 2, -this.width / 4);
            ctx.rotate(30 * Math.PI / 180);
        }
    },
    drawArcLine() {
        ctx.strokeStyle = 'rgb(0,0,0)';
        ctx.lineWidth = 0.3;
        for (var i = 0; i < 24; i++) {
            if (i == 0 || i == 6 || i == 12 || i == 18) {
                ctx.beginPath();
                ctx.moveTo(0, this.insideRadius + (this.outsideRadius - this.insideRadius) / 2);
                ctx.lineTo(0, this.outsideRadius);
            } else {
                ctx.beginPath();
                ctx.moveTo(0, this.insideRadius + (this.outsideRadius - this.insideRadius) / 2);
                ctx.lineTo(0, this.outsideRadius - this.width / 30);
            }
            ctx.stroke();
            ctx.rotate(15 * Math.PI / 180);
        }
    },
    drawArrowLine() {
        lin_ctx.strokeStyle = 'rgb(0,0,0)';
        lin_ctx.lineWidth = 3;
        lin_ctx.lineCap = 'round';
        lin_ctx.moveTo(0, -this.outsideRadius + this.width / 15);
        lin_ctx.lineTo(0, -this.outsideRadius - this.width / 20);
        lin_ctx.stroke();
    },
    subscribeCompassSensor() {
        let now = this;
        sensor.subscribeCompass({
            success: function (ret) {
                if (-now.angle - ret.direction > 1 || -now.angle - ret.direction < -1) {
                    now.angle = -ret.direction;
                    now.$emit('compassAngle', {
                        angle: ret.direction
                    });
                }
            },
            fail: function (data, code) {
                console.error('subscribe compass fail, code: ' + code + ', data: ' + data);
            },
        });
    },
    onDestroy() {
        sensor.unsubscribeCompass();
    }
}